# v 0.0 
# complejidad n^3
from timeit import default_timer as timer
import numpy as np

def printMatriz(m):
    for r in m:
        print(r)

def initMatriz(rows, columns, k):
    m = []
    for i in range(0,rows):
        m.append([k]*columns)
    return m

def productM(x, y, result):
    for i in range(len(x)):
        for j in range(len(y[0])):
            for k in range(len(y)):
                 result[i][j] += x[i][k] * y[k][j]



x = initMatriz(5,5,1)
y = initMatriz(5,5,1)
result = initMatriz(5,5,0)

#--------------------
nt = 2000
timeT = 0
times = []
for i in range(nt):
    start_t = timer()
    productM(x,y,result)
    end_t = timer()
    time = end_t - start_t
    times.append(time)
    timeT += time
media = timeT/nt


#--------------------
print(media," seconds")
print(np.mean(times)," seconds")


