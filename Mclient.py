import zmq
import queue

context = zmq.Context()
socket = context.socket(zmq.PAIR)
#socket2 = context.socket(zmq.REQ)

socket.connect("tcp://localhost:5555")
#socket2.connect("tcp://localhost:5580")


def menu():
    print ("\n    __|  MENU  |__\n")
    print ("   | List   |")
    print ("   | Next   |")
    print ("   | Play   |")
    print ("   | Exit   |")
    print ("   _______________\n")


state = True
while state:
    menu()
    option = input("Type option: ")

    if (option == 'exit'):
        state = False
    
    socket.send_string(option)
    m = socket.recv_json()
    print(m)

    








